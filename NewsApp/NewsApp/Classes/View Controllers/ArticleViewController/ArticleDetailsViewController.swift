//
//  ArticleDetailsViewController.swift
//  NewsApp
//
//  Created by Laurynas Letkauskas on 30/01/2019.
//  Copyright © 2019 Laurynas Letkauskas. All rights reserved.
//

import UIKit
import SDWebImage

class ArticleDetailsViewController: UIViewController {
    
    @IBOutlet weak var articleImage: UIImageView!
    @IBOutlet weak var articleSource: UILabel!
    @IBOutlet weak var articleTitle: UILabel!
    @IBOutlet weak var articleContent: UILabel!
    @IBOutlet weak var articleAuthor: UILabel!
    @IBOutlet weak var articleSourceURL: UILabel!
    
    private var article: Article?
    
    
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private init() {
        super.init(nibName: String(describing: ArticleDetailsViewController.self), bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    convenience init(_ article : Article) {
        self.init()
        
        self.article = article
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = articleTitle.text
        populate()
        articleContent.isUserInteractionEnabled = true
        articleContent.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapOnLabel(_:))))
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func handleTapOnLabel(_ recognizer: UITapGestureRecognizer) {
        guard let text = articleContent.attributedText?.string else {
            return
        }
        
        if let range = text.range(of: NSLocalizedString("Read more", comment: "terms")),
            recognizer.didTapAttributedTextInLabel(label: articleContent, inRange: NSRange(range, in: text)) {
            UIApplication.shared.openURL(NSURL(string: articleSourceURL.text!)! as URL)
        }
    }
    
    // MARK: helpers
    
    func populate() {
        
        if let article = self.article {
            
            
            articleImage.sd_setImage(with: URL(string: article.imageURL), completed: nil)
            articleSource.text = "Source: " + article.articleSource
            
            
            
            articleTitle.text = article.articleTitle
            articleTitle.textColor = .white
            articleTitle.textAlignment = .center
            
            
            
            articleContent.textColor = UIColor(red:0.75, green:0.75, blue:0.75, alpha:1.0)
            articleContent.text = article.articleContent.components(separatedBy: "[")[0]
            let readMore = " Read more"
            articleContent.text = articleContent.text! + readMore
            let text = (articleContent.text)!
            let underlineAttriString = NSMutableAttributedString(string: text)
            let range1 = (text as NSString).range(of: "Read more")
            underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range1)
            articleContent.attributedText = underlineAttriString
            
            
            articleAuthor.textColor = UIColor(red:0.74, green:0.74, blue:0.74, alpha:1.0)
            articleAuthor.text = "Author: " + article.articleAuthor
            
            
            
            articleSourceURL.text = article.articleURL
        }
    }
}

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x,
                                                     y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}



