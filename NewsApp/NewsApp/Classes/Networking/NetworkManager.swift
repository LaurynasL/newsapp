//
//  NetworkManager.swift
//  NewsApp
//
//  Created by Laurynas Letkauskas on 28/01/2019.
//  Copyright © 2019 Laurynas Letkauskas. All rights reserved.
//

import Alamofire
import SwiftyJSON

class NetworkManager {
    static let shared = NetworkManager()
    
    func getNewsData(url: String, completion: @escaping ([Article]) -> ()) {
        Alamofire.request(url, method: .get)
            .responseJSON { response in
                if response.result.isSuccess {
                    print("Success! Got all the articles")
                    let responseJSON : JSON = JSON(response.result.value!)
                    completion(self.updateNewsData(newsJSON: responseJSON))
                }
                else{
                    print("Error \(String(describing: response.result.error))")
                }
                
        }
    }
    
    func updateNewsData(newsJSON : JSON) -> [Article] {
        print("Parsing the JSON")
        
        var articleList = [Article]()
        
        for article in newsJSON["articles"].arrayValue {
            print(article)
            articleList.append(Article.init(articlesJSON : article))
        }
        
        
        return articleList
    }
}
