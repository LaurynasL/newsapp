//
//  MainViewController.swift
//  NewsApp
//
//  Created by Laurynas Letkauskas on 28/01/2019.
//  Copyright © 2019 Laurynas Letkauskas. All rights reserved.
//

import UIKit
import SDWebImage

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    var articleArray : [Article] = [Article]()
    
    //Cell reuse Identifier
    let cellReuseIdentifier = "cell"
    
    //MainViewControllers IBOutlets
    @IBOutlet weak var _tableView: UITableView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "NewsApp"
        _ = NetworkManager.shared
        
        _tableView.delegate = self
        _tableView.dataSource = self
        
        _tableView.register(UINib(nibName: "MainTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        _tableView.rowHeight = UITableView.automaticDimension
        _tableView.estimatedRowHeight = 2000
        
        NetworkManager.shared.getNewsData(url: "https://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=419ab7ddbe46477eae199db1d1ed3135")
            { (articleList) in
            self.articleArray = articleList
            self._tableView.reloadData()
            }
        }
    
    
    // MARK: - TableView methods
    //************************************************************************

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = _tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MainTableViewCell
        cell.selectionStyle = .none
        cell.populate(withArticle: articleArray[indexPath.row])
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigationController?.pushViewController(ArticleDetailsViewController.init(articleArray[indexPath.row]), animated: true)
    }

}

extension UINavigationController {
    
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return topViewController?.preferredStatusBarStyle ?? .lightContent
    }
}
