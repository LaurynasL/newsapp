//
//  MainTableViewCell.swift
//  NewsApp
//
//  Created by Laurynas Letkauskas on 28/01/2019.
//  Copyright © 2019 Laurynas Letkauskas. All rights reserved.
//

import UIKit
import SDWebImage

class MainTableViewCell: UITableViewCell {

    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var articleTitle: UILabel!
    @IBOutlet weak var articleDescription: UILabel!
    @IBOutlet weak var articleDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func populate(withArticle article : Article) {
        self.newsImageView.sd_setImage(with: URL(string: article.imageURL), placeholderImage: UIImage(named: "placeholder.png"))
        
        self.articleTitle.text = article.articleTitle
        self.articleTitle.textColor = .white
        self.articleTitle.textAlignment = .center


        self.articleDescription.text = article.articleDescription
        self.articleDescription.textColor = UIColor(red:0.75, green:0.75, blue:0.75, alpha:1.0)
        
        self.articleDate.text = article.articleDate
        self.articleDate.textColor = UIColor(red:0.74, green:0.74, blue:0.74, alpha:1.0)
        self.articleDate.font = UIFont.italicSystemFont(ofSize: self.articleDate.font.pointSize)
    }
    
}
