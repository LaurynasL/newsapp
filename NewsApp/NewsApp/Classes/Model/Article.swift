//
//  Article.swift
//  NewsApp
//
//  Created by Laurynas Letkauskas on 28/01/2019.
//  Copyright © 2019 Laurynas Letkauskas. All rights reserved.
//

import UIKit
import SwiftyJSON

class Article: NSObject {
    
    var articleTitle : String = ""
    var articleDescription: String = ""
    var articleContent : String = ""
    var articleAuthor : String = ""
    var articleDate : String = ""
    var articleSource : String = ""
    var articleURL : String = ""
    var imageURL : String = ""

    init(articlesJSON : JSON) {
        articleTitle = articlesJSON["title"].string!
        articleDescription = articlesJSON["description"].string!
        articleContent = articlesJSON["content"].string ?? "No article content available"
        articleAuthor = articlesJSON["author"].string ?? "Anonymous"
        articleDate = articlesJSON["publishedAt"].string!
        articleSource = articlesJSON["source"]["name"].string ?? "Unknown"
        articleURL = articlesJSON["url"].string!
        imageURL = articlesJSON["urlToImage"].string!
    }
}
